var exec = require('cordova/exec');

var printer = {
   list: function(fnSuccess, fnError){
      exec(fnSuccess, fnError, "BluetoothPrinter", "list", []);
   },
   open: function(fnSuccess, fnError, name, address){
      exec(fnSuccess, fnError, "BluetoothPrinter", "open", [name, address]);
   },
   close: function(fnSuccess, fnError){
      exec(fnSuccess, fnError, "BluetoothPrinter", "close", []);
   },
   print: function (fnSuccess, fnError, str, style, size, align) {
      exec(fnSuccess, fnError, "BluetoothPrinter", "print", [str, style, size, align]);
   }
};

module.exports = printer;
