package org.apache.cordova.g2lbluetooth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Set;
import java.util.UUID;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.BitSet;
import android.util.Base64;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

public class BluetoothPrinter extends CordovaPlugin {
	private static final String LOG_TAG = "BluetoothPrinter";
	BluetoothAdapter mBluetoothAdapter;
	BluetoothSocket mmSocket;
	BluetoothDevice mmDevice;
	OutputStream mmOutputStream;
	InputStream mmInputStream;
	Thread workerThread;
	byte[] readBuffer;
	int readBufferPosition;
	int counter;
	byte FONT_TYPE;
	byte ALIGN_TYPE;
	volatile boolean stopWorker;
	private BitSet dots;
	public BluetoothPrinter() {}
	

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		if (action.equals("list")) {
			listBT(callbackContext);
			return true;
		} else if (action.equals("open")) {
			String name = args.getString(0);
			String address = args.getString(1);
			Log.e(LOG_TAG, "Name:"+name+"... Address: "+address);
			if (findBT(callbackContext, name, address)) {
				try {
					openBT(callbackContext);
				} catch (IOException e) {
					Log.e(LOG_TAG, e.getMessage());
					e.printStackTrace();
				}
			} else {
				callbackContext.error("Bluetooth Device Not Found: " + name);
			}
			return true;
		} else if (action.equals("print")) {
			try {
				String msg = args.getString(0);
				String style = args.getString(1);
				String size = args.getString(2);
				String align = args.getString(3);
				if(style.equals("image")){
					printImage(callbackContext, msg);
				}else{
					sendData(callbackContext, msg, style, size, align);
				}
				
			} catch (IOException e) {
				Log.e(LOG_TAG, e.getMessage());
				e.printStackTrace();
			}
			return true;
		} else if (action.equals("close")) {
			try {
				closeBT(callbackContext);
			} catch (IOException e) {
				Log.e(LOG_TAG, e.getMessage());
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	void listBT(CallbackContext callbackContext) {
		BluetoothAdapter mBluetoothAdapter = null;
		String errMsg = null;
		try {
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if (mBluetoothAdapter == null) {
				errMsg = "No bluetooth adapter available";
				Log.e(LOG_TAG, errMsg);
				callbackContext.error(errMsg);
				return;
			}
			if (!mBluetoothAdapter.isEnabled()) {
				Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				this.cordova.getActivity().startActivityForResult(enableBluetooth, 0);
			}
			Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
			if (pairedDevices.size() > 0) {
				JSONArray json = new JSONArray();
				for (BluetoothDevice device : pairedDevices) {
					Hashtable map = new Hashtable();
					map.put("type", device.getType());
					map.put("address", device.getAddress());
					map.put("name", device.getName());
					JSONObject jObj = new JSONObject(map);
					json.put(jObj);
				}
				callbackContext.success(json);
			} else {
				callbackContext.error("No Bluetooth Device Found");
			}
//			Log.d(LOG_TAG, "Bluetooth Device Found: " + mmDevice.getName());
		} catch (Exception e) {
			errMsg = e.getMessage();
			Log.e(LOG_TAG, errMsg);
			e.printStackTrace();
			callbackContext.error(errMsg);
		}
	}

	// This will find a bluetooth printer device
	boolean findBT(CallbackContext callbackContext, String name,String address) {
		try {
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if (mBluetoothAdapter == null) {
				Log.e(LOG_TAG, "No bluetooth adapter available");
			}
			if (!mBluetoothAdapter.isEnabled()) {
				Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				this.cordova.getActivity().startActivityForResult(enableBluetooth, 0);
			}
			Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
			if (pairedDevices.size() > 0) {
				for (BluetoothDevice device : pairedDevices) {
					// MP300 is the name of the bluetooth printer device
					//
					if(address!=null&&address!="null"){
						if (device.getAddress().equalsIgnoreCase(address)) {
						mmDevice = device;
						Log.d(LOG_TAG, "Bluetooth Device Found: " + mmDevice.getName());
						return true;
						}
					}else{
						if (device.getName().equalsIgnoreCase(name)) {
						mmDevice = device;
						Log.d(LOG_TAG, "Bluetooth Device Found: " + mmDevice.getName());
						return true;
						}
					}
				}
			}
			
		} catch (Exception e) {
			String errMsg = e.getMessage();
			Log.e(LOG_TAG, errMsg);
			e.printStackTrace();
			callbackContext.error(errMsg);
		}
		return false;
	}

	// Tries to open a connection to the bluetooth printer device
	boolean openBT(CallbackContext callbackContext) throws IOException {
		try {
			// Standard SerialPortService ID
			UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
			mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
			mmSocket.connect();
			mmOutputStream = mmSocket.getOutputStream();
			mmInputStream = mmSocket.getInputStream();
			beginListenForData();
//			Log.d(LOG_TAG, "Bluetooth Opened: " + mmDevice.getName());
			callbackContext.success("Bluetooth Opened: " + mmDevice.getName());
			return true;
		} catch (Exception e) {
			String errMsg = e.getMessage();
			Log.e(LOG_TAG, errMsg);
			e.printStackTrace();
			callbackContext.error(errMsg);
		}
		return false;
	}

	// After opening a connection to bluetooth printer device, 
	// we have to listen and check if a data were sent to be printed.
	void beginListenForData() {
		try {
			final Handler handler = new Handler();
			// This is the ASCII code for a newline character
			final byte delimiter = 10;
			stopWorker = false;
			readBufferPosition = 0;
			readBuffer = new byte[1024];
			workerThread = new Thread(new Runnable() {
				public void run() {
					while (!Thread.currentThread().isInterrupted() && !stopWorker) {
						try {
							int bytesAvailable = mmInputStream.available();
							if (bytesAvailable > 0) {
								byte[] packetBytes = new byte[bytesAvailable];
								mmInputStream.read(packetBytes);
								for (int i = 0; i < bytesAvailable; i++) {
									byte b = packetBytes[i];
									if (b == delimiter) {
										byte[] encodedBytes = new byte[readBufferPosition];
										System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
//										final String data = new String(encodedBytes, "US-ASCII");
//										readBufferPosition = 0;
//										handler.post(new Runnable() {
//											public void run() {
//												myLabel.setText(data);
//											}
//										});
									} else {
										readBuffer[readBufferPosition++] = b;
									}
								}
							}
						} catch (IOException ex) {
							stopWorker = true;
						}
					}
				}
			});
			workerThread.start();
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * This will send data to be printed by the bluetooth printer
	 */
	boolean sendData(CallbackContext callbackContext, String msg, String style, String size, String align) throws IOException {
		try {
			byte[] printformat = { 0x1B, 0x21, FONT_TYPE };
			byte[] arrayOfByte1 = { 27, 33, 0 };
			if(style.equals("bold")){
				printformat[2] = ((byte)(0x8 | arrayOfByte1[2]));
			}
			if(style.equals("underline")){
				printformat[2] = ((byte)(0x80 | arrayOfByte1[2]));
			}
			if(size.equals("big")){
				printformat[2] = ((byte)(0x10 | arrayOfByte1[2]));
			}
			if(size.equals("bigger")){
				printformat[2] = ((byte) (0x20 | arrayOfByte1[2]));
			}
			byte[] printAlign = { 0x1B, 0x61, ALIGN_TYPE};
			if(align.equals("center")){
				printAlign[2]=49;
			}else if(align.equals("right")){
				printAlign[2]=50;
			}else{
				printAlign[2]=48;
			}
			
			mmOutputStream.write(printformat);
			mmOutputStream.write(printAlign);
			//byte[] SELECT_SPANISH_CHARACTER_CODE_TABLE = {0x1C, 0x2E, 0x1B, 0x74, 0x11};
			byte[] SELECT_SPANISH_CHARACTER_CODE_TABLE = {0x1B, 0x74, 2};
			byte[] SELECT_SPANISH_CHARACTER_CODE_TABLE2 = {0x1B, 0x52, 7};
			//String lan="\033(12N";
			
			//mmOutputStream.write(SELECT_SPANISH_CHARACTER_CODE_TABLE);
			//mmOutputStream.write(msg.getBytes());
			//mmOutputStream.write(SELECT_SPANISH_CHARACTER_CODE_TABLE2);
			//mmOutputStream.write(msg.getBytes());
			byte[] escape ={0x0a};
			
			byte[] SELECT_SPANISH_CHARACTER_CODE_TABLE16= {0x1B, 0x74, 0x06};
			mmOutputStream.write(SELECT_SPANISH_CHARACTER_CODE_TABLE16);
			mmOutputStream.write(msg.getBytes("ISO-8859-1"));
			//mmOutputStream.write(escape);
			
			byte[] printformat2 = { 0x1B, 0x21, FONT_TYPE };
			//mmOutputStream.write(printformat2);
			//mmOutputStream.write(0x0D);
			//mmOutputStream.write(0x0D);
			//mmOutputStream.write(0x0D);
			// tell the user data were sent
//			Log.d(LOG_TAG, "Data Sent");
			callbackContext.success("Data Sent");
			return true;
		} catch (Exception e) {
			String errMsg = e.getMessage();
			Log.e(LOG_TAG, errMsg);
			e.printStackTrace();
			callbackContext.error(errMsg);
		}
		return false;
	}
	private void printImage(CallbackContext callbackContext, String image) {
			byte[] FEED_LINE = {10};
			byte[] SET_LINE_SPACING_24 = {0x1B, 0x33, 24};
			byte[] SET_LINE_SPACING_30 = {0x1B, 0x33, 30};
			byte widthLSB = (byte)(380 & 0xFF);
			byte widthMSB = (byte)((380 >> 8) & 0xFF);
			byte[] SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33, (byte) widthLSB, widthMSB};
	    	try {
	    	
	        mmOutputStream.write(SET_LINE_SPACING_24);
			mmOutputStream.write(FEED_LINE);
			String[] items = image.split("base64,");
			byte[] decodedString = Base64.decode(items[1], Base64.DEFAULT);
			Bitmap decoded = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
			
			
			Bitmap bmOverlay = Bitmap.createBitmap(decoded.getWidth(), decoded.getHeight(), decoded.getConfig());
	        Bitmap bmp2 =Bitmap.createBitmap(decoded.getWidth(), decoded.getHeight(), decoded.getConfig());
	        bmp2.eraseColor(Color.WHITE);
		    Canvas canvas = new Canvas(bmOverlay);
	        canvas.drawBitmap(bmp2, new Matrix(), null);
	        canvas.drawBitmap(decoded, new Matrix(), null);
			final int maxSize = 380;
			int outWidth;
			int outHeight;
			int inWidth = bmp2.getWidth();
			int inHeight = bmp2.getHeight();
			if(inWidth > inHeight){
				outWidth = maxSize;
				outHeight = (inHeight * maxSize) / inWidth; 
			} else {
				outHeight = maxSize;
				outWidth = (inWidth * maxSize) / inHeight; 
			}

			Bitmap bmp=Bitmap.createScaledBitmap(bmOverlay, outWidth, outHeight, false);
			convertBitmap(bmp);
	        int offset = 0;
	        while (offset < bmp.getHeight()) {
				
				
	        	mmOutputStream.write(SELECT_BIT_IMAGE_MODE);
	            for (int x = 0; x < bmp.getWidth(); ++x) {

	                for (int k = 0; k < 3; ++k) {

	                    byte slice = 0;
	                    for (int b = 0; b < 8; ++b) {
	                        int y = (((offset / 8) + k) * 8) + b;
	                        int i = (y * bmp.getWidth()) + x;
	                        boolean v = false;
	                        if (i < dots.length()) {
	                            v = dots.get(i);
	                        }
	                        slice |= (byte) ((v ? 1 : 0) << (7 - b));
	                    }
	                    mmOutputStream.write(slice);
	                }
	            }
	            offset += 24;
	            mmOutputStream.write(FEED_LINE);
	        }
	        mmOutputStream.write(SET_LINE_SPACING_30);
			callbackContext.success("Image printed!");
	    	} catch (IOException e) {
				String errMsg = e.getMessage();
				Log.e(LOG_TAG, errMsg);
				e.printStackTrace();
				callbackContext.error(errMsg+" - "+image);
			}
	}
	 public String convertBitmap(Bitmap inputBitmap) {

		    int mWidth = inputBitmap.getWidth();
		    int mHeight = inputBitmap.getHeight();

		    convertArgbToGrayscale(inputBitmap, mWidth, mHeight);
		    String mStatus = "ok";
		    return mStatus;

		}

		private void convertArgbToGrayscale(Bitmap bmpOriginal, int width,
		        int height) {
		    int pixel;
		    int k = 0;
		    int B = 0, G = 0, R = 0;
		    dots = new BitSet();
		    try {

		        for (int x = 0; x < height; x++) {
		            for (int y = 0; y < width; y++) {
		                // get one pixel color
		                pixel = bmpOriginal.getPixel(y, x);

		                // retrieve color of all channels
		                R = Color.red(pixel);
		                G = Color.green(pixel);
		                B = Color.blue(pixel);
		                // take conversion up to one single value by calculating
		                // pixel intensity.
		                R = G = B = (int) (0.299 * R + 0.587 * G + 0.114 * B);
		                // set bit into bitset, by calculating the pixel's luma
		                if (R < 195) {                       
		                    dots.set(k);//this is the bitset that i'm printing
		                }
		                k++;

		            }


		        }


		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
	// Close the connection to bluetooth printer.
	boolean closeBT(CallbackContext callbackContext) throws IOException {
		try {
			stopWorker = true;
			mmOutputStream.close();
			mmInputStream.close();
			mmSocket.close();
//			myLabel.setText("Bluetooth Closed");
			callbackContext.success("Bluetooth Closed");
			return true;
		} catch (Exception e) {
			String errMsg = e.getMessage();
			Log.e(LOG_TAG, errMsg);
			e.printStackTrace();
			callbackContext.error(errMsg);
		}
		return false;
	}


	public byte[] convertExtendedAscii(String input)
{
        int length = input.length();
    byte[] retVal = new byte[length];

    for(int i=0; i<length; i++)
    {
              char c = input.charAt(i);

              if (c < 127)
              {
                      retVal[i] = (byte)c;
              }
              else
              {
                      retVal[i] = (byte)(c - 256);
              }
    }

    return retVal;
}
}